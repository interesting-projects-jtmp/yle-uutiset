# Names

[Kuinka retro etunimesi on? Tee nostalgiamatka Ylen nimikoneella](https://yle.fi/uutiset/3-9418798)

## Author

Teemo Tebest (Yle)

## Description

Finnish first names and amounts from 1900 to 2016.

## Data format

.csv (UTF-8)